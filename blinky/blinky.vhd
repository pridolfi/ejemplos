library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity blinky is
generic (T_PERIOD_MS : integer :=500);
port(
        rst: in std_logic;
        clk: in std_logic;
        led_out: out std_logic
);
end blinky;

architecture blinky_arch of blinky is
    constant CLOCK_FREQ: integer :=12000000;
    constant COUNT_MAX : integer :=(CLOCK_FREQ/1000) * T_PERIOD_MS;
    signal count       : unsigned (31 downto 0);
begin

  counter: process (clk)
    begin
      if (rising_edge(clk)) then
        if(rst='0' or count=COUNT_MAX) then
          count <= (others => '0');
        else
          count <= count + 1;
        end if;
      end if;
    end process;

    led_out <= '0' when count<COUNT_MAX/2 else
               '1';

end architecture blinky_arch;
