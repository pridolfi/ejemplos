# Acerca de este ejemplo

Es el ejemplo minimo descrito en el documento VUnit.md

Muestra los elementos indispensables para usar VUnit :
 - Un script run.py
 - Uno o mas testbench en VHDL (o Verilog) que usen la libreria VUnit_lib

## Partes importantes del codigo

### run.py

Es el script en Python con el paso a paso para importar y usar VUnit:

```python
# Importar librerias de VUnit
from vunit import VUnit

# Crear un contexto VUnit con los comandos pasados desde la Terminal
vu = VUnit.from_argv()

# Crear libreria de tests
lib = vu.add_library("lib")

# Agregar todos los archivos terminados en '.vhdl' al espacio de trabajo de VUnit
# La herramienta reconoce como Testbenches aquellos que:
# 1. Empiezan con tb_*
# 2. Terminan con *_tb
lib.add_source_files("*.vhdl")

# Ejecutar VUnit
vu.main()
```

### tb_base.vhdl

Es un testbench basico, que sirve como esqueleto para implementar tests compatibles con VUnit.
Las lineas mas importantes estan indicadas con comentarios:
```vhdl
-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

entity tb_base is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end entity;

architecture tb of tb_base is
begin
  main : process
  begin

    --Iniciar testbench
    test_runner_setup(runner, runner_cfg);

    --Aqui va el contenido del testbench
    report "Hello world!";

    --Terminar testbench
    test_runner_cleanup(runner);
  end process;
end architecture;
```

## Instrucciones de uso

Abrir un Terminal y ejecutar:
```bash
# Para una version Python 3.x, con x>=6:
python3.x run.py
```

Aparece un resumen con el resultado de las pruebas ejecutadas:
```
Starting lib.tb_example.all
Output file: vunit_out/test_output/lib.tb_example.all_7b5933c73ddb812488c059080644e9fd58c418d9/output.txt
pass (P=1 S=0 F=0 T=1) lib.tb_example.all (0.4 seconds)

==== Summary ==============================
pass lib.tb_example.all (0.4 seconds)
===========================================
pass 1 of 1
===========================================
Total time was 0.4 seconds
Elapsed time was 0.4 seconds
===========================================
```

Si vamos a **vunit_out/test_output/lib.tb_example.all_7b5933c73ddb812488c059080644e9fd58c418d9/output.txt**, encontramos
los datos generados por el Testbench:
```
/home/ramiro/FPGA/VUnit/Base_example/tb_example.vhdl:21:5:@0ms:(report note): Hello world!
simulation stopped @0ms with status 0
```
