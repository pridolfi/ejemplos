# Acerca de este ejemplo

Este ejemplo muestra como VUnit puede detectar, correr y resumir multiples pruebas combinadas 
en un mismo archivo de testbench. Esto es util para agrupar tests similares o relacionados. Por
ejemplo, en un testbench se pueden agrupar varias pruebas para distintas velocidades y formatos
de trama de un periferico UART.

## Partes importantes del codigo

El archivo **tb_combinado.vhdl** se basa en el mismo esqueleto que los ejemplos anteriores,
pero ahora el **test_runner** de VUnit abarca un bucle **loop** que va recorriendo 
distintos tests:

```vhdl
main : process
  begin
    test_runner_setup(runner, runner_cfg);

    -- Se hace un recorrido por varios tests distintos
    while test_suite loop

      -- Primer test : pasa
      if run("test_pass") then
        report "This will pass";

      -- Segundo test : no pasa
      elsif run("test_fail") then
        assert false report "You shall not pass !!!" severity error;

      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
```

## Instrucciones de uso

Abrir un Terminal y ejecutar:
```bash
# Para una version Python 3.x, con x>=6:
python3.x run.py
```

Aparece un resumen con el resultado de las pruebas ejecutadas:
```bash
fail (P=1 S=0 F=1 T=2) lib.tb_combinado.test_fail (0.5 seconds)

==== Summary ======================================
pass lib.tb_combinado.test_pass (0.5 seconds)
fail lib.tb_combinado.test_fail (0.5 seconds)
===================================================
pass 1 of 2
fail 1 of 2
===================================================
Total time was 0.9 seconds
Elapsed time was 0.9 seconds
===================================================
Some failed!
```

Podemos ver cuantas pruebas pasaron y cuantas fallaron del conjunto.
Notese que ambas pertenecen al mismo archivo de prueba (lib.tb_combinado.x)
pero difieren en su nombre especifico (lib.tb_combinado.test_pass y lib.tb_combinado.test_fail).

