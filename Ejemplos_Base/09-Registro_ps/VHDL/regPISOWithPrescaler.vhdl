--------------------------------------------------------------------------------
--- Entidad: regPISOWithPrescaler.
--- Descripción: Esta entidad es la implementación de un registro PISO de 4 bits
--				 hecha especialmente para la EDU-CIAA-FPGA. El registro se
--				 compone de un registro PISO interno de entidad "regPISO" de 4
--				 bits más un contador universal "univCounter" utilizado como
--				 prescaler para reducir los 12 MHz de la placa a 1 Hz. Para
--				 que el contador pueda dividir la frecuencia necesita un
--				 módulo de 12 000 000, por lo que se lo instancia con 24 bits.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 14/09/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;					--Biblioteca estándar ieee.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.

--Declaración de la entidad.
entity regPISOWithPrescaler is
	generic(
			nBitsReg 		   : integer := 4;
			risingEdgeClockReg : BOOLEAN := TRUE);
	port(
		d_in 			: in  std_logic_vector(nBitsReg-1 downto 0);
		load_in 		: in  std_logic;
		reset_in 		: in  std_logic;
		outEnable_in	: in  std_logic;
		clock_in 		: in  std_logic;
		clockEnable_in 	: in  std_logic;
		q_out 			: out std_logic);
end entity regPISOWithPrescaler;

--Declaración de la arquitectura.
architecture regPISOWithPrescaler_arch of regPISOWithPrescaler is
	--Declaración del registro PISO interno.
	component regPISO is
	  generic (
		nBits           : integer := 8;
		risingEdgeClock : BOOLEAN := TRUE
	  );
	  port (
		d_in           : in  std_logic_vector(nBits-1 downto 0);
		load_in        : in  std_logic;
		reset_in       : in  std_logic;
		outEnable_in   : in  std_logic;
		clock_in       : in  std_logic;
		clockEnable_in : in  std_logic;
		q_out          : out std_logic
	  );
	end component;

	--Declaración del contador interno que se usa como prescaler.
	component univCounter is
	  generic (
		nBits           : integer := 8;
		modulus         : integer := 256;
		risingEdgeClock : BOOLEAN := TRUE
	  );
	  port (
		d_in              : in  std_logic_vector(nBits-1 downto 0);
		clock_in          : in  std_logic;
		outEnable_in      : in  std_logic;
		reset_in          : in  std_logic;
		counterEnable_in  : in  std_logic;
		load_in           : in  std_logic;
		countUp_in        : in  std_logic;
		q_out             : out std_logic_vector(nBits-1 downto 0);
		terminalCount_out : out std_logic
	  );
	end component;

	--Declaración de constantes para el contador interno.
	constant COUNTER_NBITS   : integer := 24;
	constant COUNTER_MODULUS : integer := 12000000;

	--Declaración de señales internas para conexiones del registro PISO interno.
	signal registerLoad_ena       : std_logic;
	signal register_rst 	      : std_logic;
	signal register_clk 	      : std_logic;
	signal registerInputData_s    : std_logic_vector(nBitsReg-1 downto 0);
	signal registerOutputData_s   : std_logic;
	signal registerClock_ena      : std_logic;
	signal registerOut_ena	      : std_logic;

	--Declaración de señales internas para conexiones del contador interno.
	signal counterLoad_ena	      : std_logic := '0';
	signal counterUpCount_ena     : std_logic := '1';
	signal counterClock_ena       : std_logic;
	signal counterTerminalCount_s : std_logic;

begin
	--Instanciación del registro PISO interno.
	regPISO_0 : regPISO
		generic map ( nBits => nBitsReg, risingEdgeClock => risingEdgeClockReg)
		port map ( d_in           => registerInputData_s,
		  		   load_in        => registerLoad_ena,
		  		   reset_in       => register_rst,
		  		   outEnable_in   => registerOut_ena,
		  		   clock_in       => register_clk,
		  		   clockEnable_in => registerClock_ena,
		  		   q_out          => registerOutputData_s);

	--Instanciación del contador interno que se usa como prescaler.
	univCounter_0 : univCounter
	generic map ( nBits           => COUNTER_NBITS,
				  modulus         => COUNTER_MODULUS,
	  	  		  risingEdgeClock => risingEdgeClockReg)
	port map ( d_in              => open,
	  		   clock_in          => register_clk,
	  		   outEnable_in      => registerOut_ena,
	  		   reset_in          => register_rst,
	  		   counterEnable_in  => counterClock_ena,
	  		   load_in           => counterLoad_ena,
	  		   countUp_in        => counterUpCount_ena,
	  		   q_out             => open,
	  		   terminalCount_out => counterTerminalCount_s);

	--Se hacen las conexiones internas.
	registerInputData_s <= d_in;
	registerLoad_ena    <= load_in;
	register_clk        <= clock_in;
	register_rst        <= reset_in;
	registerOut_ena     <= outEnable_in;
	counterClock_ena 	<= clockEnable_in;
	registerClock_ena 	<= counterClock_ena and counterTerminalCount_s;
	q_out 			    <= registerOutputData_s;
end architecture regPISOWithPrescaler_arch;
