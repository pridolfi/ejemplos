package parametersPackage is
    constant NCOBITS : integer :=  10;
    constant FREQCONTROLBITS : integer := 3;
    constant DATABITS : integer := 8;
    constant BRDIVISOR : integer := 26;
end parametersPackage;
