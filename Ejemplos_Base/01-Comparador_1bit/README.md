# COMPARADOR DE MAGNITUD DE 1 BIT

[[_TOC_]]

## 1. Descripción

En este ejemplo se diseña un comparador lógico de 1 bit. Este caso sencillo 
permite poner en práctica el diseño de circuitos digitales a "nivel de 
compuertas" (*gate-level*), por lo que se usan operaciones lógicas sencillas del
HDL.

Un comparador de magnitud es un dispositivo digital **combinacional** que tiene dos 
entradas (en este caso de 1 bit) y tres salidas de 1 bit cada una, y cuya 
función es analizar si sus entradas son iguales o diferentes. Esto se puede ver 
en las siguientes imágenes:

![Comparador-diagrama](.images/Block Diagram.png)
![Comparador-tabla](.images/Truth table.jpg)

Se observa como cada salida se pone en alto cuando se cumple su condición:

*  A = B
*  A > B
*  A < B 

## 2. Código

### 2.1. VHDL

Primero que nada, se incluyen los paquetes necesarios, que por el momento es
solo el *std_logic_1164* para usar los tipos de datos **std_logic** y 
**std_logic_vector**.

```vhdl
--Inclusión de paquetes.
library ieee; 			--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector
```

Luego, se declara la **entidad** del comparador. Esta solo tendrá los pines de I/O
del dispositivo.

```vhdl
--Declaración de la entidad.
entity comp1bit is
	port(
		a_in   : in std_logic;
		b_in   : in std_logic;
		eq_out : out std_logic;
		gt_out : out std_logic;
		lt_out : out std_logic
	);
end entity comp1bit;
```
Los nombres de las salidas se eligieron con el siguiente criterio:
* eq : equal (igual).
* gt : greater than (mayor que).
* lt : less than (menor que).

Finalmente, se declara la **arquitectura** del comparador. Basándose en la tabla 
de verdad antes vista, se asignan las salidas según su primer forma canónica, es 
decir, según la suma lógica de los "minitérminos" que les correspondan. 

Para que el código quede más legible, se usan señales auxiliares para el caso
de la salida por igualdad (A = B).
```vhdl
--Declaración de la arquitectura.
architecture comp1bit_arch of comp1bit is
	--Declaración de señales auxiliares que representan minitérminos.
	signal p0_s	: std_logic;
	signal p1_s	: std_logic;
begin
	--Obtención de cada minitérmino.
	p0_s <= (not a_in) and (not b_in);
	p1_s <= a_in and b_in;

	--Cada salida se asigna según la primer forma canónica.
	eq_out <= p0_s or p1_s;
	lt_out <= (not a_in) and b_in;
	gt_out <= a_in and (not b_in);
end architecture comp1bit_arch;
```

Para ver todo el **código** del módulo y el *testbench* en VHDL, ir [aquí](VHDL).


## 3. Simulación

Como se trata de un **circuito combinacional**, se diseña un *testbench* para probar
de **forma exhaustiva** al comparador. En otras palabras, se le van a inyectar todas
las combinaciones posibles de las entradas y se verificarán los valores de las 
salidas, tal cual aparece en la tabla de verdad.

Una vez hecho el *testbench*, se realizará la simulación utilizando el *toolchain*
correspondiente. Para más información, ver sección 6 del presente documento.

### 3.1. Resultados

En la siguiente imagen se aprecian las formas de onda visualizadas con gtkWave.

![Simulacion-VHDL](.images/Simulation_VHDL.png)

Se observa como las salidas del comparador cumplen con el comportamiento 
deseado, quedando así verificado el diseño.

## 4. Implementación y ensayo

Para implementar el diseño en la [EDU-FPGA](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/EDU-FPGA) 
se debe tener en cuenta lo siguiente:
* ¿Cómo voy a estimular al comparador?
* ¿Cómo voy a ver el estado de las salidas? 

Como en muchos ejemplos simples, para solventar este dilema se hace uso de los 
**pulsadores** y los **LED** embebidos en la placa. Para este caso, la 
asignación de pines es:

|  **Componente embebido**  |  **Pin FPGA**     |   **Pin comparador**  |
|:-------------------------:|:----------------: |:---------------------:|
|  Pulsador 1               |       31          |   a_in                |
|  Pulsador 2               |       32          |   b_in                |
|  LED 1                    |       1           |   eq_out              |
|  LED 2      		        |       2           |   gt_out              |
|  LED 3                    |       3           |   lt_out              |

Para ver el archivo **.pcf** ir [aquí](comp1bit_pcf_sbt.pcf), y para el pinout de la EDU-FPGA ir [aquí](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Pinout/Pinout%20EDU%20FPGA.pdf).

**Pendiente: Agregar video de ensayo**:warning:

---

Se motiva al lector/a a que diseñe, simule e implemente un comparador de magnitud 
de 2 bits utilizando como base el módulo creado del comparador de 1 bit.

## 5. Véase también

### 5.1. Sintaxis relacionada

* [Sintaxis básica VHDL](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Sintaxis-VHDL)
* [Testbenches en VHDL](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Testbenches-en-VHDL)

- [Sintaxis básica en Verilog](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Sintaxis-Verilog)
- [Testbenches en Verilog](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Testbenches-en-Verilog)

### 5.2. Ejemplos relacionados

1. [Compuerta AND](../00-Compuerta_AND)

## 6. Información adicional: uso de herramientas

Para más información sobre cómo usar el *toolchain* para compilar y simular o 
descargar a la placa según el lenguaje utilizado, referirse a los siguientes 
vínculos:

 - VHDL y Verilog (recomendado): [Entorno basado en Atom][Entorno_Link].
 - Solo VHDL : [GHDL y GTKWave](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Herramientas-libres-para-VHDL) + [iCEcube2](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Software-Lattice) + [IceStorm][IceStorm_link]
 - Solo Verilog : [icarusVerilog y GTKWave](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Herramientas-libres-para-Verilog) + [IceStorm][IceStorm_link]

[Entorno_Link]: https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Uso-del-entorno
[IceStorm_link]: https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Proyecto-iCEstorm
